/*
 * ServicesIRCh - Services for IRCh, timeout.h
 *
 * Copyright (C) 2019 Toni Garcia - zoltan <toni@tonigarcia.es>
 * Copyright (C) 1996-1999 Andy Church <achurch@achurch.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#if !defined(TIMEOUT_H)
#define TIMEOUT_H

#include <time.h>


/* Definitions for timeouts: */
typedef struct timeout_ Timeout;
struct timeout_ {
    Timeout *next, *prev;
    time_t settime, timeout;
    int repeat;			/* Does this timeout repeat indefinitely? */
    void (*code)(Timeout *);	/* This structure is passed to the code */
    void *data;			/* Can be anything */
};


/* Check the timeout list for any pending actions. */
extern void check_timeouts(void);

/* Add a timeout to the list to be triggered in `delay' seconds.  Any
 * timeout added from within a timeout routine will not be checked during
 * that run through the timeout list.
 */
extern Timeout *add_timeout(int delay, void (*code)(Timeout *), int repeat);

/* Remove a timeout from the list (if it's there). */
extern void del_timeout(Timeout *t);

#ifdef DEBUG_COMMANDS
/* Send the list of timeouts to the given user. */
extern void send_timeout_list(User *u);
#endif


#endif	/* TIMEOUT_H */
